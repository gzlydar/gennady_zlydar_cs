import simple_draw as sd
sd.resolution = (1150, 900)


def triangle():
    point = sd.get_point(250, 150)
    v1 = sd.get_vector(start_point=point, angle=0, length=150, width=2)
    v1.draw(color=sd.COLOR_RED)
    v2 = sd.get_vector(start_point=v1.end_point, angle=120, length=150, width=2)
    v2.draw(color=sd.COLOR_RED)
    v3 = sd.get_vector(start_point=v2.end_point, angle=240, length=150, width=2)
    v3.draw(color=sd.COLOR_RED)


triangle()


def square():
    point = sd.get_point(500, 150)
    v1 = sd.get_vector(start_point=point, angle=0, length=150, width=2)
    v1.draw(color=sd.COLOR_RED)
    v2 = sd.get_vector(start_point=v1.end_point, angle=90, length=150, width=2)
    v2.draw(color=sd.COLOR_RED)
    v3 = sd.get_vector(start_point=v2.end_point, angle=180, length=150, width=2)
    v3.draw(color=sd.COLOR_RED)
    v4 = sd.get_vector(start_point=v3.end_point, angle=270, length=150, width=2)
    v4.draw(color=sd.COLOR_RED)


square()


def pentagon():
    point = sd.get_point(900, 150)
    v1 = sd.get_vector(start_point=point, angle=0, length=150, width=2)
    v1.draw(color=sd.COLOR_RED)
    v2 = sd.get_vector(start_point=v1.end_point, angle=72, length=150, width=2)
    v2.draw(color=sd.COLOR_RED)
    v3 = sd.get_vector(start_point=v2.end_point, angle=144, length=150, width=2)
    v3.draw(color=sd.COLOR_RED)
    v4 = sd.get_vector(start_point=v3.end_point, angle=216, length=150, width=2)
    v4.draw(color=sd.COLOR_RED)
    v5 = sd.get_vector(start_point=v4.end_point, angle=288, length=150, width=2)
    v5.draw(color=sd.COLOR_RED)


pentagon()


def hexagon():
    point = sd.get_point(450, 500)
    v1 = sd.get_vector(start_point=point, angle=0, length=150, width=2)
    v1.draw(color=sd.COLOR_RED)
    v2 = sd.get_vector(start_point=v1.end_point, angle=60, length=150, width=2)
    v2.draw(color=sd.COLOR_RED)
    v3 = sd.get_vector(start_point=v2.end_point, angle=120, length=150, width=2)
    v3.draw(color=sd.COLOR_RED)
    v4 = sd.get_vector(start_point=v3.end_point, angle=180, length=150, width=2)
    v4.draw(color=sd.COLOR_RED)
    v5 = sd.get_vector(start_point=v4.end_point, angle=240, length=150, width=2)
    v5.draw(color=sd.COLOR_RED)
    v6 = sd.get_vector(start_point=v5.end_point, angle=300, length=150, width=2)
    v6.draw(color=sd.COLOR_RED)


hexagon()

sd.pause()
