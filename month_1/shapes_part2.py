import simple_draw as sd

sd.resolution = (1200, 800)
sd.background_color = sd.COLOR_WHITE


def figure(point=sd.get_point(50, 100), angle=0, length=150, width=2, delta=120, sides=3):  # треугольник
    if angle == delta * sides:
        return
    v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=width)
    v1.draw(color=sd.COLOR_RED)
    next_point = v1.end_point
    next_angle = angle + delta
    sd.sleep(0.01)
    figure(point=next_point, angle=next_angle, length=length, width=width, delta=delta)


figure()


def figure(point=sd.get_point(250, 100), angle=0, length=150, width=2, delta=90, sides=4):  # квадрат
    if angle == delta * sides:
        return
    v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=width)
    v1.draw(color=sd.COLOR_RED)
    next_point = v1.end_point
    next_angle = angle + delta
    sd.sleep(0.01)
    figure(point=next_point, angle=next_angle, length=length, width=width, delta=delta)


figure()


def figure(point=sd.get_point(500, 100), angle=0, length=150, width=2, delta=72, sides=5):  # пятиугольник
    if angle == delta * sides:
        return
    v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=width)
    v1.draw(color=sd.COLOR_RED)
    next_point = v1.end_point
    next_angle = angle + delta
    sd.sleep(0.01)
    figure(point=next_point, angle=next_angle, length=length, width=width, delta=delta)


figure()


def figure(point=sd.get_point(800, 100), angle=0, length=150, width=2, delta=60, sides=6):  # шестиугольник
    if angle == delta * sides:
        return
    v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=width)
    v1.draw(color=sd.COLOR_RED)
    next_point = v1.end_point
    next_angle = angle + delta
    sd.sleep(0.01)
    figure(point=next_point, angle=next_angle, length=length, width=width, delta=delta)


figure()


def figure(point=sd.get_point(100, 300), angle=0, length=150, width=2, delta=51.57, sides=7):  # семиугольник
    if angle >= delta * sides:
        return
    v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=width)
    v1.draw(color=sd.COLOR_RED)
    next_point = v1.end_point
    next_angle = angle + delta
    sd.sleep(0.01)
    figure(point=next_point, angle=next_angle, length=length, width=width, delta=float(delta))


figure()


def figure(point=sd.get_point(500, 400), angle=0, length=150, width=2, delta=45, sides=8):  # восьмиугольник
    if angle == delta * sides:
        return
    v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=width)
    v1.draw(color=sd.COLOR_RED)
    next_point = v1.end_point
    next_angle = angle + delta
    sd.sleep(0.01)
    figure(point=next_point, angle=next_angle, length=length, width=width, delta=delta)


figure()


def figure(point=sd.get_point(900, 600), angle=0, length=40, width=3, delta=40, sides=9):  # девятиугольник
    if angle == delta * sides:
        return
    v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=width)
    v1.draw(color=sd.COLOR_RED)
    next_point = v1.end_point
    next_angle = angle + delta
    sd.sleep(0.01)
    figure(point=next_point, angle=next_angle, length=length, width=width, delta=delta)


figure()


def figure(point=sd.get_point(900, 400), angle=0, length=80, width=1, delta=72, sides=100):  # балуюсь;)
    if angle >= delta * sides:
        return
    v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=width)
    v1.draw(color=sd.COLOR_RED)
    next_point = v1.end_point
    next_angle = angle + delta
    sd.sleep(0.01)
    figure(point=next_point, angle=next_angle, length=length, width=width, delta=delta)


figure()
sd.pause()
